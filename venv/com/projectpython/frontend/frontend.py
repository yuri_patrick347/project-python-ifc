from config import *
from model import Produto
import requests 

@app.route("/")
def inicio():
    return 'Lista Produtos '+\
        '<a href="/listar_produtos">Operação listar</a>'

@app.route("/listar_produtos")
def listar_produtos():
    # OBTER OS PRODUTOS DO BACK-END
    resultado_requisicao = requests.get('http://127.0.0.1:5000/listar_produtos')
    # DADOS JSON PODEM SER CARREGADOS EM DICIONÁRIOS DO PYTHON
    json_produtos = resultado_requisicao.json() 
    # INICIALIZA UMA LISTA DO PYTHON
    produtos_em_python = []
    # PERCORRER OS PRODUTOS EM JSON
    for p in json_produtos:
        # CRIA UM PRODUTO PASSANDO AS INFORMAÇÕES DO DICIONÁRIO
        produto = Produto(**p)
        # ADICIONAR O PRODUTO CONVERTIDO NA LISTA DE PRODUTO
        produtos_em_python.append(produto)
    
    # FORNECER A LISTA DE PRODUTO PARA A PÁGINA E EXIBIR OS PRODUTOS
    return render_template("listar_produtos.html", listagem = produtos_em_python)

app.run(debug=True, port=4999)

app.run()