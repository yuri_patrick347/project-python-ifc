$(function() { // quando o documento estiver pronto/carregado
    
    // função para listar produtos na tabela
    function listar_produtos() {
        $.ajax({
            url: 'http://127.0.0.1:5000/listar_produtos',
            method: 'GET',
            dataType: 'json', // os dados são recebidos no formato json
            success: listar, // chama a função listar para processar o resultado
            error: function() {
                alert("erro ao ler dados, verifique o backend");
            }
        });
        function listar (produtos) {
            // esvaziar o corpo da tabela
            $('#corpoTabelaProdutos').empty();
            // tornar a tabela visível
            mostrar_conteudo("tabelaProdutos");      
            // percorrer a lista de produtos retornadas; 
            for (var i in produtos) { //i vale a posição no vetor
                lin = '<tr id="linha_'+produtos[i].id+'">' + 
                '<td>' + produtos[i].nome + '</td>' + 
                '<td>' + produtos[i].preco + '</td>' + 
                '<td>' + produtos[i].descricao + '</td>' + 
                '<td><a href=# id="excluir_' + produtos[i].id + '" ' + 
                  'class="excluir_produto"> Excluir '+
                '</td>' + 
                '</tr>';
                // adiciona a linha no corpo da tabela
                $('#corpoTabelaProdutos').append(lin);
            }
        }
    }

    // função que mostra um conteúdo e esconde os outros
    function mostrar_conteudo(identificador) {
        // esconde todos os conteúdos
        $("#tabelaProdutos").addClass('invisible');
        $("#conteudoInicial").addClass('invisible');
        // torna o conteúdo escolhido visível
        $("#"+identificador).removeClass('invisible');      
    }

    // código para mapear o click do link Listar
    $(document).on("click", "#linkListarProdutos", function() {
        listar_produtos();
    });
    
    // código para mapear click do link Inicio
    $(document).on("click", "#linkInicio", function() {
        mostrar_conteudo("conteudoInicial");
    });

    // código para mapear click do botão incluir pessoa
    $(document).on("click", "#btCadastrarProduto", function() {
        //pegar dados da tela
        nome = $("#campProduto").val();
        preco = $("#campoPreco").val();
        descricao = $("#campoDescricao").val();
        // preparar dados no formato json
        var dados = JSON.stringify({ nome: nome, preco: preco, descricao: descricao});
        // fazer requisição para o back-end
        $.ajax({
            url: 'http://127.0.0.1:5000/adicionar_produto',
            type: 'POST',
            dataType: 'json', // os dados são recebidos no formato json
            contentType: 'application/json', // tipo dos dados enviados
            data: dados, // estes são os dados enviados
            success: produtoCadastrado, // chama a função listar para processar o resultado
            error: erroAoCadastrar
        });
        function produtoCadastrado (retorno) {
            if (retorno.resultado == "ok") { // a operação deu certo?
                // informar resultado de sucesso
                alert("Produto cadastrado com sucesso!");
                // limpar os campos
                $("#campProduto").val("");
                $("#campoPreco").val("");
                $("#campoDescricao").val("");
            } else {
                // informar mensagem de erro
                alert(retorno.resultado + ":" + retorno.detalhes);
            }            
        }
        function erroAoCadastrar (retorno) {
            // informar mensagem de erro
            alert("ERRO: "+retorno.resultado + ":" + retorno.detalhes);
        }
    });

    // código a ser executado quando a janela de inclusão de pessoas for fechada
    $('#modalIncluirProduto').on('hide.bs.modal', function (e) {
        // se a página de listagem não estiver invisível
        if (! $("#tabelaProdutos").hasClass('invisible')) {
            // atualizar a página de listagem
            listar_produtos();
        }
    });

    // a função abaixo é executada quando a página abre
    mostrar_conteudo("conteudoInicial");

 // código para os ícones de excluir produto (classe css)
 $(document).on("click", ".excluir_produto", function() {
    // obter o ID do ícone que foi clicado
    var componente_clicado = $(this).attr('id'); 
    // no id do ícone, obter o ID do produto
    var nome_icone = "excluir_";
    var id_produto = componente_clicado.substring(nome_icone.length);
    // solicitar a exclusão do produto
    $.ajax({
        url: 'http://localhost:5000/excluir_produto/'+id_produto,
        type: 'DELETE', // método da requisição
        dataType: 'json', // os dados são recebidos no formato json
        success: produtoExcluido, // chama a função listar para processar o resultado
        error: erroAoExcluir
    });
    function produtoExcluido (retorno) {
        if (retorno.resultado == "ok") { 
            $("#linha_" + id_produto).fadeOut(1000, function(){
                alert("Produto removida com sucesso!");
            });
        } else {
            // informar mensagem de erro
            alert(retorno.resultado + ":" + retorno.detalhes);
        }            
    }
    function erroAoExcluir (retorno) {
        // informar mensagem de erro
        alert("erro ao excluir dados, verifique o backend: ");
    }
});

});
