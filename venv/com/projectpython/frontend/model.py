from config import * 


class Produto(db.Model):
    #ATRIBUTOS DO PRODUTO
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(254))
    preco = db.Column(db.String(254))

    #CONSTRUTOR
    def __str__(self):
        return str(self.id)+") "+ self.nome + ", " + self.preco

    #FUNCAO PARA RETORNAR JSON
    def json(self):
        return {
            "id": self.id,
            "nome": self.nome,
            "preco": self.preco
        }

#TESTE
if __name__ == "__main__":  
    #CRIAR TABELA
    db.create_all()

    #TESTE DA CLASSE PRODUTO
    p1 = Produto(nome="Parafuso", preco="R$ 1.00")
    p2 = Produto(nome="Furadeira", preco="R$ 10.00")
    p3 = Produto(nome="Martelo", preco="R$ 5.00")
    p4 = Produto(nome="Prego", preco="R$ 2.00")

    #PERSISTIR
    db.session.add(p1)
    db.session.add(p2)
    db.session.add(p3)
    db.session.add(p4)
    db.session.commit()

    #EXIBIR PRODUTO NO FORMATO JSON
    print(p1.json)
