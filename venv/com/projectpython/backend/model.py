from config import * 

#Produto
class Produto(db.Model):
    #ATRIBUTOS DO PRODUTO
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(254))
    preco = db.Column(db.String(254))
    descricao = db.Column(db.String(254))

    #CONSTRUTOR
    def __str__(self):
        return str(self.id)+") "+ self.nome + ", " + self.preco + ", " + self.descricao

    def json(self):
        return {
            "id": self.id,
            "nome": self.nome,
            "preco": self.preco,
            "descricao": self.descricao
        }
#Categoria
class Categoria(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nomeCategoria = db.Column(db.String(254))    

    #atributo de chave estrangeira
    produto_id = db.Column(db.Integer, db.ForeignKey(Produto.id), nullable=False) 
    # atributo de relacionamento, para acesso aos dados via objeto 
    produto = db.relationship("Produto")

    def __str__(self): # expressão da classe em forma de texto 
      return  self.nomeCategoria + " " + \
         str(self.produto) # o str aciona o __str__ da classe Pessoa 

    def json(self): 
      return { 
         "id":self.id, 
         "Nome Categoria":self.nomeCategoria, 
         "produto_id":self.produto_id, 
         "produto":self.produto.json() 
      }

#USUARIO
 #ATRIBUTOS DO USUARIO
class Usuario(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(254))
    email = db.Column(db.String(254))
    phone = db.Column(db.String(254))
    password = db.Column(db.String(254))

#CONSTRUTOR
    def __str__(self):
        return str(self.id)+") "+ self.nome + ", " + self.email + ", " + self.phone + ", " + self.password
    
    def json(self):
        return {
            "id": self.id,
            "E-mail": self.email,
            "Telefone": self.phone,
            "Senha": self.password
        }

#PEDIDO
 #ATRIBUTOS DO PEDIDO
class Pedido(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    momento = db.Column(db.String(254))
    orderStatus = db.Column(db.String(254))

 #atributo de chave estrangeira
    usuario_id = db.Column(db.Integer, db.ForeignKey(Usuario.id), nullable=False) 
    # atributo de relacionamento, para acesso aos dados via objeto 
    usuario = db.relationship("Usuario")

    #CONSTRUTOR
    def __str__(self):
        return str(self.id)+") "+ self.momento + ", " + self.orderStatus + ", " + str(self.usuario)

    def json(self): 
      return { 
         "id":self.id, 
         "Momento":self.momento,
         "Status do Pedido":self.orderStatus, 
         "usuario_id":self.usuario_id, 
         "usuario":self.usuario.json() 
      }

#ITEM
 #ATRIBUTOS DO ITEM
class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    quantidade = db.Column(db.String(254))
    preco = db.Column(db.String(254))

    #atributo de chave estrangeira
    pedido_id = db.Column(db.Integer, db.ForeignKey(Pedido.id), nullable=False) 
    # atributo de relacionamento, para acesso aos dados via objeto 
    pedido = db.relationship("Pedido")

    #atributo de chave estrangeira
    produto_id = db.Column(db.Integer, db.ForeignKey(Produto.id), nullable=False) 
    # atributo de relacionamento, para acesso aos dados via objeto 
    produto = db.relationship("Produto")

    #CONSTRUTOR
    def __str__(self):
        return str(self.id)+") "+ self.quantidade + ", " + self.preco + ", " + str(self.pedido) + str(self.produto)

    def json(self): 
      return { 
         "id":self.id, 
         "Quantidade":self.quantidade,
         "Preco":self.preco, 
         "pedido_id":self.pedido_id, 
         "pedido":self.pedido.json(), 
         "produto_id":self.produto_id, 
         "produto":self.produto.json() 
      }
    
#PAGAMENTO
 #ATRIBUTOS DO PAGAMENTO
class Pagamento(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    momento = db.Column(db.String(254))

    #atributo de chave estrangeira
    pedido_id = db.Column(db.Integer, db.ForeignKey(Pedido.id), nullable=False) 
    # atributo de relacionamento, para acesso aos dados via objeto 
    pedido = db.relationship("Pedido")

    #CONSTRUTOR
    def __str__(self):
        return str(self.id)+") "+ self.momento + ", " + str(self.pedido)

    def json(self): 
      return { 
         "id":self.id, 
         "Momento":self.momento,
         "pedido_id":self.pedido_id, 
         "pedido":self.pedido.json() 
      }

#TESTE
if __name__ == "__main__":
    #APAGAR O ARQUIVO, SE HOUVER
    if os.path.exists(arquivobd):
        os.remove(arquivobd)
    
    #CRIAR TABELA
    db.create_all()

    #TESTE DA CLASSE PRODUTO
    p1 = Produto(nome="Parafuso", preco="R$ 1,00", descricao="3mm")
    p2 = Produto(nome="Furadeira", preco="R$ 10,00", descricao="3mm")

    #TESTE DA CLASSE CATEGORIA
    cat1 = Categoria(produto=p1, nomeCategoria="Ferramenta")

    #TESTE DA CLASSE USUARIO
    u1 = Usuario(nome="Joao", email="joao@gmail.com", phone="98944545", password="55454")

    #TESTE DA CLASSE PEDIDO
    ped1 = Pedido(momento="10/06/2021 - 10:00", orderStatus="PAGO", usuario=u1)

    #TESTE DA CLASSE ITEM
    i1 = Item(quantidade="4", preco="R$ 3,00", pedido=ped1, produto=p1)

    #TESTE DA CLASSE ITEM
    pag1 = Pagamento(momento="10/06/2021 - 11:00", pedido=ped1)

    

    #PERSISTIR
    db.session.add(p1)
    db.session.add(cat1) 
    db.session.add(u1)
    db.session.add(ped1)
    db.session.add(i1)
    db.session.add(pag1)

    db.session.commit()

     #EXIBIR
    print(f"Produto json: {p1.json()}")  
    print(f"Categoria json: {cat1.json()}")  
    print(f"Usuario json: {u1.json()}") 
    print(f"Pedido json: {ped1.json()}")  
    print(f"Item json: {i1.json()}")  
    print(f"Pagamento json: {pag1.json()}")  

    