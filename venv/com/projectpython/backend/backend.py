from config import *
from model import Produto
from model import Categoria
from model import Usuario
from model import Pedido
from model import Item
from model import Pagamento

@app.route("/")
def inicio():
    return 'Sistema de cadastro de produtos '+\
        '<a href="/listar_produtos"> Operação listar</a>'

@app.route("/listar_produtos")
def listar_produtos():
    # obter os produtos do cadastro
    produtos = db.session.query(Produto).all()
    # aplicar o método json que a classe Produto possui a cada elemento da lista
    produtos_em_json = [ x.json() for x in produtos ]
    # converter a lista do python para json
    resposta = jsonify(produtos_em_json)
     # PERMITIR resposta para outras pedidos oriundos de outras tecnologias
    resposta.headers.add("Access-Control-Allow-Origin", "*")
    return resposta # retornar...

# teste da rota: curl -d '{"nome":"Parafuso", "preco":"R$ 1.00"}' -X POST -H "Content-Type:application/json" localhost:5000/incluir_pessoa
@app.route("/adicionar_produto", methods=['post'])
def adicionar_produto():
    # preparar uma resposta otimista
    resposta = jsonify({"resultado": "ok", "detalhes": "ok"})
    # receber as informações do novo produto
    dados = request.get_json() #(force=True) dispensa Content-Type na requisição
    try: # tentar executar a operação
      addProduto = Produto(**dados) # criar novo produto
      db.session.add(addProduto) # adicionar no BD
      db.session.commit() # efetivar a operação de gravação
    except Exception as e: # em caso de erro...
      # informar mensagem de erro
      resposta = jsonify({"resultado":"erro", "detalhes":str(e)})
    # adicionar cabeçalho de liberação de origem
    resposta.headers.add("Access-Control-Allow-Origin", "*")
    return resposta # responder!

@app.route("/listar_categorias") 
# o código da função abaixo ésimilar ao código da função listar_produto 
# será que dava pra generalizar essa função? :-) 
def listar_categorias(): 
   # obter exames realizados 
   categorias = db.session.query(Categoria).all() 
   # converter dados para json 
   lista_jsons = [ x.json() for x in categorias ] 
   # converter a lista do python para json 
   resposta = jsonify(lista_jsons) 
   # PERMITIR resposta para outras pedidos oriundos de outras tecnologias 
   resposta.headers.add("Access-Control-Allow-Origin", "*") 
   return resposta

@app.route("/listar/<string:classe>") 

def listar(classe): 
   # obter os dados da classe informada 
   dados = None 
   if classe == "Categoria": 
    dados = db.session.query(Categoria).all() 
   elif classe == "Produto": 
    dados = db.session.query(Produto).all() 
   # converter dados para json 
   lista_jsons = [ x.json() for x in dados ] 
   # converter a lista do python para json 
   resposta = jsonify(lista_jsons) 
   # PERMITIR resposta para outras pedidos oriundos de outras tecnologias 
   resposta.headers.add("Access-Control-Allow-Origin", "*") 
   return resposta

@app.route("/excluir_produto/<int:produto_id>", methods=['DELETE']) 
def excluir_produto(produto_id): 
   # preparar uma resposta otimista 
   resposta = jsonify({"resultado": "ok", "detalhes": "ok"}) 
   try: 
      # excluir produto do ID informado 
      Produto.query.filter(Produto.id == produto_id).delete() 
      # confirmar a exclusão 
      db.session.commit() 
   except Exception as e: 
      # informar mensagem de erro 
      resposta = jsonify({"resultado":"erro", "detalhes":str(e)}) 
   # adicionar cabeçalho de liberação de origem 
   resposta.headers.add("Access-Control-Allow-Origin", "*") 
   return resposta # responder!

app.run(debug=True)
